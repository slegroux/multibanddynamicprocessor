//
//  CompressedGain.h
//  musicDSPProjWeibin
//
//  Created by Weibin Shen on 4/24/13.
//  Copyright (c) 2013 Weibin Shen. All rights reserved.
//

#ifndef __musicDSPProjWeibin__CompressedGain__
#define __musicDSPProjWeibin__CompressedGain__

#include <iostream>
#include "AGBiquad.h"
#define TAV 0.01
#define min(X, Y)  ((X) < (Y) ? (X) : (Y))

class CompressedGain{
public:
    CompressedGain();
    ~CompressedGain();
    
    double obtainGain(double input);

    void setCenterFrequency(double newF0);
    void setQ(double newQ);
    void setRatio(double newRatio);
    void setThresh(double newThresh);
    void setAttack(double newAttack);
    void setRelease(double newRelease);
    void setMakeUpGain(double newMakeUpGain);
    void setSamplerate(double newSampleRate);
private:
    double F0;
    double Q;
    double ratio;
    double thresh;
    double attack;
    double release;
    double makeUpGain;
    double sampleRate;
    
    double xrms;
    double g;
    
    AGBiquad * bpf;
    
    double bandPassFilter(double input);
    
};

#endif /* defined(__musicDSPProjWeibin__CompressedGain__) */
