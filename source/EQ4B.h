//
//  EQ4B.h
//  musicDSPProjWeibin
//
//  Created by Shaoduo Xie on 4/24/13.
//  Copyright (c) 2013 Weibin Shen. All rights reserved.
//

#ifndef __musicDSPProjWeibin__EQ4B__
#define __musicDSPProjWeibin__EQ4B__

#include <iostream>
#include "AGBiquad.h"

#define NUM_BANDS 4

class EQ4B
{
public:
    EQ4B(double *fcs, double *qs, double *gs);
    ~EQ4B() {}
    
    double process(double input);
    
    void updateGain(double *g);
    
    void updateGain(int index, double g);
    
    void reset();
    
    AGBiquad biquads0[NUM_BANDS];
};


#endif /* defined(__musicDSPProjWeibin__EQ4B__) */
