//
//  EQ4B.cpp
//  musicDSPProjWeibin
//
//  Created by Shaoduo Xie on 4/24/13.
//  Copyright (c) 2013 Weibin Shen. All rights reserved.
//

#include "EQ4B.h"

//=====================================================
EQ4B::EQ4B(double *fcs, double *qs, double *gs)
{
    biquads0[0].calc_filter_coeffs(5, fcs[0], 44100, qs[0], gs[0], true);
    biquads0[1].calc_filter_coeffs(4, fcs[1], 44100, qs[1], gs[1], true);
    biquads0[2].calc_filter_coeffs(4, fcs[2], 44100, qs[2], gs[2], true);
    biquads0[3].calc_filter_coeffs(6, fcs[3], 44100, qs[3], gs[3], true);
}

double EQ4B::process(double input)
{
    double output0 = (double)input;
    for (int i = 0; i < NUM_BANDS; ++i) {
        output0 = biquads0[i].process(output0);
    }
    
    return output0;
}

void EQ4B::updateGain(double *g)
{
    for (int i = 0; i < NUM_BANDS; ++i) {
        biquads0[i].calc_filter_coeffs(biquads0[i].type, biquads0[i].freq, 44100, biquads0[i].bw, g[i], true);
    }
}

void EQ4B::updateGain(int index, double g)
{
    biquads0[index].calc_filter_coeffs(biquads0[index].type, biquads0[index].freq, 44100, biquads0[index].bw, g, true);
}

void EQ4B::reset()
{
    for (int i = 0; i < NUM_BANDS; ++i) {
        biquads0[i].reset();
    }
}

