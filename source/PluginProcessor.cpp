/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic startup code for a Juce application.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"


//==============================================================================
C4_newTypeAudioProcessor::C4_newTypeAudioProcessor()
{
    mcL = new MultibandCompressor();
    mcR = new MultibandCompressor();
  
    counter = 0;
//    startTimer(100);
}

C4_newTypeAudioProcessor::~C4_newTypeAudioProcessor()
{
    if (mcL)
        delete mcL;
    if (mcR)
        delete mcR;
}

//==============================================================================
const String C4_newTypeAudioProcessor::getName() const
{
    return JucePlugin_Name;
}

int C4_newTypeAudioProcessor::getNumParameters()
{
    return 0;
}

float C4_newTypeAudioProcessor::getParameter (int index)
{
    return 0.0f;
}

void C4_newTypeAudioProcessor::setParameter (int index, float newValue)
{
}

const String C4_newTypeAudioProcessor::getParameterName (int index)
{
    return String::empty;
}

const String C4_newTypeAudioProcessor::getParameterText (int index)
{
    return String::empty;
}

const String C4_newTypeAudioProcessor::getInputChannelName (int channelIndex) const
{
    return String (channelIndex + 1);
}

const String C4_newTypeAudioProcessor::getOutputChannelName (int channelIndex) const
{
    return String (channelIndex + 1);
}

bool C4_newTypeAudioProcessor::isInputChannelStereoPair (int index) const
{
    return true;
}

bool C4_newTypeAudioProcessor::isOutputChannelStereoPair (int index) const
{
    return true;
}

bool C4_newTypeAudioProcessor::acceptsMidi() const
{
   #if JucePlugin_WantsMidiInput
    return true;
   #else
    return false;
   #endif
}

bool C4_newTypeAudioProcessor::producesMidi() const
{
   #if JucePlugin_ProducesMidiOutput
    return true;
   #else
    return false;
   #endif
}

bool C4_newTypeAudioProcessor::silenceInProducesSilenceOut() const
{
    return false;
}

double C4_newTypeAudioProcessor::getTailLengthSeconds() const
{
    return 0.0;
}

int C4_newTypeAudioProcessor::getNumPrograms()
{
    return 0;
}

int C4_newTypeAudioProcessor::getCurrentProgram()
{
    return 0;
}

void C4_newTypeAudioProcessor::setCurrentProgram (int index)
{
}

const String C4_newTypeAudioProcessor::getProgramName (int index)
{
    return String::empty;
}

void C4_newTypeAudioProcessor::changeProgramName (int index, const String& newName)
{
}

//==============================================================================
void C4_newTypeAudioProcessor::prepareToPlay (double sampleRate, int samplesPerBlock)
{
    // Use this method as the place to do any pre-playback
    // initialisation that you need..
}

void C4_newTypeAudioProcessor::releaseResources()
{
    // When playback stops, you can use this as an opportunity to free up any
    // spare memory, etc.
}

void C4_newTypeAudioProcessor::processBlock (AudioSampleBuffer& buffer, MidiBuffer& midiMessages)
{
    // This is the place where you'd normally do the guts of your plugin's
    // audio processing...
    
    float **lr = buffer.getArrayOfChannels();
    float *left     = NULL;
    float *right    = NULL;
    unsigned int numSamples = buffer.getNumSamples();
    for (int i = 0; i < numSamples; ++i) {
        left    = &(lr[0][i]);
        right   = &(lr[1][i]);
        
        float sampleL = *left;
        float sampleR = *right;
        
        double outputL = mcL->process(sampleL);
        double outputR = mcR->process(sampleR);
        
        *left = (float)outputL;
        *right = (float)outputR;
        
        if (++counter == 4410) {
            counter = 0;
            if (hasEditor()) {
                C4_newTypeAudioProcessorEditor *e = (C4_newTypeAudioProcessorEditor*)getActiveEditor();
                if (e) {
                    for (int i = 0; i < NUM_BANDS; ++i) {
                        e->dummyBiquads[i].a0 = mcL->eq->biquads0[i].a0;
                        e->dummyBiquads[i].a1 = mcL->eq->biquads0[i].a1;
                        e->dummyBiquads[i].a2 = mcL->eq->biquads0[i].a2;
                        e->dummyBiquads[i].b0 = mcL->eq->biquads0[i].b0;
                        e->dummyBiquads[i].b1 = mcL->eq->biquads0[i].b1;
                        e->dummyBiquads[i].b2 = mcL->eq->biquads0[i].b2;
                    }
                    e->handleAsyncUpdate();
                }
            }
        }
    }
    
//    for (int channel = 0; channel < getNumInputChannels(); ++channel)
//    {
//        float* channelData = buffer.getSampleData (channel);
//
//        // ..do something to the data...
//        for (int i = 0; i < buffer.getNumSamples(); ++i) {
//            float sample = channelData[i];
//            double output = mc[channel].process(sample);
//            channelData[i] = (float)output;
//        }
//    }

    // In case we have more outputs than inputs, we'll clear any output
    // channels that didn't contain input data, (because these aren't
    // guaranteed to be empty - they may contain garbage).
    for (int i = getNumInputChannels(); i < getNumOutputChannels(); ++i)
    {
        buffer.clear (i, 0, buffer.getNumSamples());
    }
    
    //getActiveEditor()->repaint();
}

void C4_newTypeAudioProcessor::timerCallback()
{
    if (hasEditor()) {
        C4_newTypeAudioProcessorEditor *e = (C4_newTypeAudioProcessorEditor*)getActiveEditor();
        if (e) {
            e->handleAsyncUpdate();
        }
    }
    
}

//==============================================================================
bool C4_newTypeAudioProcessor::hasEditor() const
{
    return true; // (change this to false if you choose to not supply an editor)
}

AudioProcessorEditor* C4_newTypeAudioProcessor::createEditor()
{
    return new C4_newTypeAudioProcessorEditor (this);
}

//==============================================================================
void C4_newTypeAudioProcessor::getStateInformation (MemoryBlock& destData)
{
    // You should use this method to store your parameters in the memory block.
    // You could do that either as raw data, or use the XML or ValueTree classes
    // as intermediaries to make it easy to save and load complex data.
}

void C4_newTypeAudioProcessor::setStateInformation (const void* data, int sizeInBytes)
{
    // You should use this method to restore your parameters from this memory block,
    // whose contents will have been created by the getStateInformation() call.
}

//==============================================================================
// This creates new instances of the plugin..
AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    return new C4_newTypeAudioProcessor();
}
