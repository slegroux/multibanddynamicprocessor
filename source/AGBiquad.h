
#ifndef AG_BIQUAD
#define AG_BIQUAD

class AGBiquad
{
public:
    AGBiquad();
    ~AGBiquad();
    
    float process(float in0);
    
    void calc_filter_coeffs(int type,double frequency, double sample_rate, double q,double db_gain,bool q_is_bandwidth);
    
    void reset();
    
    float   freq;
	float   bw;
	float   gain;
    int     type;

    double alpha, a0, a1, a2, b0, b1, b2;
	// filter coeffs
	float b0a0, b1a0, b2a0, a1a0, a2a0;
	// in/out history
	float ou1, ou2, in1, in2;
};

#endif