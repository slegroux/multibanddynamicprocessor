#include "AGBiquad.h"
#include <math.h>

AGBiquad::AGBiquad()
{
    // reset filter coeffs
    b0a0 = b1a0 = b2a0 = a1a0 = a2a0 = 0.0;
    // reset in/out history
    ou1 = ou2 = in1 = in2 = 0.0f;	
    freq = 0.0;
    type = 1; //high pass
}

AGBiquad::~AGBiquad()
{
    
}

float AGBiquad::process(float in0)
{
    float const yn = b0a0*in0 + b1a0*in1 + b2a0*in2 - a1a0*ou1 - a2a0*ou2;
    in2 = in1;
    in1 = in0;
    ou2 = ou1;
    ou1 = yn;    
    return yn;
}

void AGBiquad::calc_filter_coeffs(int type, double frequency, double sample_rate, double q, double db_gain, bool q_is_bandwidth)
{
    this->type = type;
    freq = (float)frequency;
    bw = (float)q;
    gain = (float)db_gain;
    // temp pi
    double const temp_pi = 3.1415926535897932384626433832795;
    // temp coef vars
    alpha = a0 = a1 = a2 = b0 = b1 = b2 = 0.0;
    
    // peaking, lowshelf and hishelf
    if(type>=4)
    {
        double const A		=	pow(10.0,(db_gain/40.0));
        double const omega	=	2.0*temp_pi*frequency / sample_rate;
        double const tsin	=	sin(omega);
        double const tcos	=	cos(omega);
        
        if(q_is_bandwidth)
			alpha=tsin*sinh(log(2.0)/2.0*q*omega/tsin);
        else
			alpha=tsin/(2.0*q);
        
        double const beta	=	sqrt(A)/q;

        if(type==4) { // peaking
            b0=float(1.0+alpha*A);
            b1=float(-2.0*tcos);
            b2=float(1.0-alpha*A);
            a0=float(1.0+alpha/A);
            a1=float(-2.0*tcos);
            a2=float(1.0-alpha/A);
        }
        else if(type==5) { // lowshelf
            b0=float(A*((A+1.0)-(A-1.0)*tcos+beta*tsin));
            b1=float(2.0*A*((A-1.0)-(A+1.0)*tcos));
            b2=float(A*((A+1.0)-(A-1.0)*tcos-beta*tsin));
            a0=float((A+1.0)+(A-1.0)*tcos+beta*tsin);
            a1=float(-2.0*((A-1.0)+(A+1.0)*tcos));
            a2=float((A+1.0)+(A-1.0)*tcos-beta*tsin);
        }
        else if(type==6) { // hishelf
            b0=float(A*((A+1.0)+(A-1.0)*tcos+beta*tsin));
            b1=float(-2.0*A*((A-1.0)+(A+1.0)*tcos));
            b2=float(A*((A+1.0)+(A-1.0)*tcos-beta*tsin));
            a0=float((A+1.0)-(A-1.0)*tcos+beta*tsin);
            a1=float(2.0*((A-1.0)-(A+1.0)*tcos));
            a2=float((A+1.0)-(A-1.0)*tcos-beta*tsin);
        }
    }
    else {
        // other filters
        double const omega	=	2.0*temp_pi*frequency / sample_rate;
        double const tsin	=	sin(omega);
        double const tcos	=	cos(omega);
        
        if(q_is_bandwidth)
			alpha=tsin*sinh(log(2.0)/2.0*q*omega/tsin);
        else
			alpha=tsin/(2.0*q);
        
        // lowpass
        if(type==0) {
            b0=(1.0-tcos)/2.0;
            b1=1.0-tcos;
            b2=(1.0-tcos)/2.0;
            a0=1.0+alpha;
            a1=-2.0*tcos;
            a2=1.0-alpha;
        }
        else if(type==1) { // hipass
            b0=(1.0+tcos)/2.0;
            b1=-(1.0+tcos);
            b2=(1.0+tcos)/2.0;
            a0=1.0+ alpha;
            a1=-2.0*tcos;
            a2=1.0-alpha;
        }
        else if(type==2) { // bandpass czpg
            b0=alpha;
            b1=0.0;
            b2=-alpha;
            a0=1.0+alpha;
            a1=-2.0*tcos;
            a2=1.0-alpha;
        }
    }
    // set filter coeffs
    b0a0 = float(b0/a0);
    b1a0 = float(b1/a0);
    b2a0 = float(b2/a0);
    a1a0 = float(a1/a0);
    a2a0 = float(a2/a0);
}

void AGBiquad::reset()
{
    in1 = in2 = ou1 = ou2 = 0.0;
}