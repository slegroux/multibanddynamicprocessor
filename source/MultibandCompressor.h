//
//  MultibandCompressor.h
//  musicDSPProjWeibin
//
//  Created by Weibin Shen on 4/24/13.
//  Copyright (c) 2013 Weibin Shen. All rights reserved.
//

#ifndef __musicDSPProjWeibin__MultibandCompressor__
#define __musicDSPProjWeibin__MultibandCompressor__

#define NUM_OF_BANDS 4
#define MIN_Q 0.1

#include <iostream>
#include "CompressedGain.h"
#include "EQ4B.h"


class MultibandCompressor{
public:
    MultibandCompressor();
    ~MultibandCompressor();
    
    double process(double input);
    
    void setFcr(size_t index, double newValue);
    void setRatio(size_t index, double newValue);
    void setThresh(size_t index, double newValue);
    void setAttack(size_t index, double newValue);
    void setRelease(size_t index, double newValue);
    void setMakeUpGain(size_t index, double newValue);
    void setSampleRate(double newSampleRate);
    
    EQ4B *eq;
private:
    
    double * Fcrs;//cross over frequencies, length = 3
    
    double * Fcs;//central frequencies, length = 4
    double * Qs;//Q values, length = 4
    double * ratios;//ratio values, length = 4
    double * threshs;//thresh values, length = 4;
    double * attacks;//attack values, length = 4
    double * releases;//release values, length = 4;
    double * makeUpGains;//make up gain values, length = 4;
    
    double sampleRate;
    
    CompressedGain * gainGenerators;//totally four of them, just to generate the gain values
    
    
};

#endif /* defined(__musicDSPProjWeibin__MultibandCompressor__) */
