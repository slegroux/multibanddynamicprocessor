//
//  CompressedGain.cpp
//  musicDSPProjWeibin
//
//  Created by Weibin Shen on 4/24/13.
//  Copyright (c) 2013 Weibin Shen. All rights reserved.
//

#include "CompressedGain.h"
#include "math.h"

CompressedGain::CompressedGain(){
    F0 = 1000;//Hz
    Q = 1;
    ratio = 1;
    thresh = 0;//dB
    attack = 30;//ms
    release = 3;//ms
    makeUpGain = 0;//dB
    sampleRate = 44100;
    bpf = new AGBiquad();
    bpf->calc_filter_coeffs(2,F0, sampleRate, Q,0,false);
    xrms = 0;
    g = 1;
}

CompressedGain::~CompressedGain(){
    delete bpf;
}

double CompressedGain::bandPassFilter(double input){
    return bpf->process(input);
}

double CompressedGain::obtainGain(double input){
    double sample = bandPassFilter(input);
    double tav_sample = 1 - pow(2.718, -2.2 / sampleRate / TAV);
    xrms = (1-tav_sample) * xrms + tav_sample * sample * sample;
    double X = 10 * log10(xrms);
    double G = min(0,(1 - 1.0/ratio) * (thresh - X));
    double f = pow(10, G/20.);
    double coeff;
    if(f<g){
        coeff = 1 - pow(2.718, -2.2 / sampleRate / (attack / 1000.));
    }else{
        coeff = 1 - pow(2.718, -2.2 / sampleRate / (release / 1000.));
    }
    g = (1-coeff) * g + coeff * f;
    return g * pow(10.,makeUpGain/20.);
}

void CompressedGain::setCenterFrequency(double newF0){
    F0 = newF0;
    bpf->calc_filter_coeffs(2,F0, sampleRate, Q,0,false);
}

void CompressedGain::setQ(double newQ){
    Q = newQ;
    bpf->calc_filter_coeffs(2,F0, sampleRate, Q,0,false);
}

void CompressedGain::setRatio(double newRatio){
    ratio = newRatio;
}

void CompressedGain::setThresh(double newThresh){
    thresh = newThresh;
}

void CompressedGain::setAttack(double newAttack){
    attack = newAttack;
}

void CompressedGain::setRelease(double newRelease){
    release = newRelease;
}

void CompressedGain::setMakeUpGain(double newMakeUpGain){
    makeUpGain = newMakeUpGain;
}

void CompressedGain::setSamplerate(double newSampleRate){
    sampleRate = newSampleRate;
    bpf->calc_filter_coeffs(2,F0, sampleRate, Q,0,false);
}
