//
//  MultibandCompressor.cpp
//  musicDSPProjWeibin
//
//  Created by Weibin Shen on 4/24/13.
//  Copyright (c) 2013 Weibin Shen. All rights reserved.
//

#include "MultibandCompressor.h"
#include "math.h"

MultibandCompressor :: MultibandCompressor(){
    eq = NULL;
    
    Fcrs = new double[NUM_OF_BANDS - 1];
    Fcs = new double[NUM_OF_BANDS];
    Qs = new double[NUM_OF_BANDS];;
    ratios = new double[NUM_OF_BANDS];
    threshs = new double[NUM_OF_BANDS];
    attacks = new double[NUM_OF_BANDS];
    releases = new double[NUM_OF_BANDS];
    makeUpGains = new double[NUM_OF_BANDS];
    sampleRate = 44100;
    
    gainGenerators = new CompressedGain[NUM_OF_BANDS];
    
    if(NUM_OF_BANDS == 4){
        setFcr(0, 500);
        setFcr(1, 2000);
        setFcr(2, 8000);
    }
//    setMakeUpGain(0, -12);
//    setMakeUpGain(1, 12);
//    setMakeUpGain(2, -12);
//    setMakeUpGain(3, 12);
    
    makeUpGains[0] = 0;
    makeUpGains[1] = 0;
    makeUpGains[2] = 0;
    makeUpGains[3] = 0;
    eq = new EQ4B(Fcs, Qs, makeUpGains);
    
    for(size_t i = 0; i < NUM_OF_BANDS; i++){
        setRatio(i, 1);
        setThresh(i, 0);
        setAttack(i, 30);
        setRelease(i, 3);
        setMakeUpGain(i, 0);
    }
    
}

MultibandCompressor :: ~MultibandCompressor(){
    delete[] Fcrs;
    delete[] Fcs;
    delete[] Qs;
    delete[] ratios;
    delete[] threshs;
    delete[] attacks;
    delete[] releases;
    delete[] makeUpGains;
    delete[] gainGenerators;
    delete eq;
}

double safeDevide(double v1, double v2){
    double ret = v1/v2;
    if(isnan(ret)){
        ret = MIN_Q;
    }else if(fabs(ret) < MIN_Q){
        ret = signbit(ret) ? -MIN_Q : MIN_Q;
    }
    return ret;
}

void MultibandCompressor :: setFcr(size_t index, double newValue){
    if(index < NUM_OF_BANDS - 1){
        Fcrs[index] = newValue;
        if(index == 0){
            Fcs[index] = sqrt(Fcrs[index] * 20);
            Qs[index] = safeDevide((Fcrs[index] + 20)/2, Fcrs[index] - 20) * Q_factor;
        }else if(index == NUM_OF_BANDS - 1){
            Fcs[index] = sqrt(Fcrs[index-1] * 20000);
            Qs[index] = safeDevide((20000 + Fcrs[index - 1])/2, 20000 - Fcrs[index - 1]) * Q_factor;
        }else{
            Fcs[index] = sqrt(Fcrs[index] * Fcrs[index-1]);
            Qs[index] = safeDevide((Fcrs[index] + Fcrs[index - 1])/2, Fcrs[index] - Fcrs[index - 1]) * Q_factor;
        }
        
        gainGenerators[index].setCenterFrequency(Fcs[index]);
        gainGenerators[index].setQ(Qs[index]);
        
        //update Qs[index] and Fcs[index]
        if (eq)
            eq->updateQandFc(index, Qs[index], Fcs[index]);
        
        if(index + 1 ==  NUM_OF_BANDS - 1){
            Fcs[index + 1] = sqrt(Fcrs[index] * 20000);
            Qs[index + 1] = safeDevide((20000 + Fcrs[index])/2, 20000 - Fcrs[index]) * Q_factor;
        }else if(index < NUM_OF_BANDS - 1){
            Fcs[index + 1] = sqrt(Fcrs[index + 1] * Fcrs[index]);
            Qs[index + 1] = safeDevide((Fcrs[index + 1] + Fcrs[index])/2, Fcrs[index + 1] - Fcrs[index]) * Q_factor;
        }
        
        gainGenerators[index + 1].setCenterFrequency(Fcs[index + 1]);
        gainGenerators[index + 1].setQ(Qs[index + 1]);
        
        if (eq)
            eq->updateQandFc(index + 1, Qs[index + 1], Fcs[index + 1]);
    }
    
    
}
void MultibandCompressor :: setRatio(size_t index, double newValue){
    if(index >= NUM_OF_BANDS)
        return;
    ratios[index] = newValue;
    gainGenerators[index].setRatio(newValue);
}
void MultibandCompressor :: setThresh(size_t index, double newValue){
    if(index >= NUM_OF_BANDS)
        return;
    threshs[index] = newValue;
    gainGenerators[index].setThresh(newValue);
}
void MultibandCompressor :: setAttack(size_t index, double newValue){
    if(index >= NUM_OF_BANDS)
        return;
    attacks[index] = newValue;
    gainGenerators[index].setAttack(newValue);
}
void MultibandCompressor :: setRelease(size_t index, double newValue){
    if(index >= NUM_OF_BANDS)
        return;
    releases[index] = newValue;
    gainGenerators[index].setRelease(newValue);
}
void MultibandCompressor :: setMakeUpGain(size_t index, double newValue){
    if(index >= NUM_OF_BANDS)
        return;
    makeUpGains[index] = newValue;
    gainGenerators[index].setMakeUpGain(newValue);
//    double gains = gainGenerators[index].obtainGain(1.0);
//    std::cout << "obtain gain: " << gainGenerators[index].obtainGain(1.0) << std::endl;
//    double gs[4] = {1.0, 1.0, 1.0, 1.0};
//    eq->updateGain(gs);
//    std::cout << "eq: " << eq->biquads0[1].gain << " " << eq->biquads0[1].freq << std::endl;
    //eq->updateGain(index, newValue);
    for (int i = 0; i < 4; ++i) {
        std::cout << "q: " << Qs[i] << " " << Fcs[i] << std::endl;
    }
}

void MultibandCompressor::setSampleRate(double newSampleRate){
    sampleRate = newSampleRate;
    for (size_t i = 0; i < NUM_OF_BANDS; i++) {
        gainGenerators[i].setSamplerate(newSampleRate);
    }
}

double MultibandCompressor::process(double input){
    double gains[NUM_OF_BANDS];
    for (size_t i = 0; i < NUM_OF_BANDS; i++) {
        double g = gainGenerators[i].obtainGain(input);
        eq->updateGain(i, 20 * log10(g));
    }
    
    //eq->updateGain();
    double output = eq->process(input);
    return output;
    //shaoduo: put the EQ processsing here.
    //The center frequencies are in Fcs[], gains are in gains[], Qs are in Qs[]
}
